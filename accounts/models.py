import re
import uuid

from django.db import models
from django.core import validators
from django.contrib.auth.models import AbstractBaseUser, UserManager, \
    PermissionsMixin


class User(AbstractBaseUser, PermissionsMixin):

    username = models.CharField(max_length=30, unique=True, validators=[
            validators.RegexValidator(
                re.compile('^[\w.@+-]+$'),
                'Informe um nome de usuário válido. '
                'Este valor deve conter apenas letras, números '
                'e os caracteres: @/./+/-/_ .', 'invalid'
            )
        ],
        help_text='Um nome curto que será usado para'
                  'identificá-lo de forma única na plataforma'
    )
    email = models.EmailField('E-mail', unique=True)
    is_staff = models.BooleanField('Equipe', default=False)
    is_active = models.BooleanField('Ativo', default=True)
    date_joined = models.DateTimeField('Data de Entrada', auto_now_add=True)
    jwt_secret = models.UUIDField(default=uuid.uuid4)
    
    class Meta:
        verbose_name = 'Usuário'
        verbose_name_plural = 'Usuários'

    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = ['email']

    objects = UserManager()

    def __str__(self):
        return self.username


def jwt_get_secret_key(user_model):
     return user_model.jwt_secret