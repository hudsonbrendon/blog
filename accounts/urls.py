from django.urls import path

from .views import UserLogoutAPIView, UserListAPIView, UserRetrieveAPIView, UserCreateAPIView, UserUpdateAPIView, UserDestroyAPIView

from rest_framework_jwt import views as jwt_views


urlpatterns = [
    path('login/', jwt_views.ObtainJSONWebToken.as_view(), name='users_login'),
    path('login/refresh/', jwt_views.RefreshJSONWebToken.as_view(), name='users_login_refresh'),
    path('logout/', UserLogoutAPIView.as_view(), name='users_logout'),
    path('', UserListAPIView.as_view(), name='users'),
    path('<int:pk>/', UserRetrieveAPIView.as_view(), name='users_detail'),
    path('create/', UserCreateAPIView.as_view(), name='users_create'),
    path('update/<int:pk>/', UserUpdateAPIView.as_view(), name='users_update'),
    path('delete/<int:pk>/', UserDestroyAPIView.as_view(), name='users_delete'),
]
