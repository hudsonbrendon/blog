import json
from urllib.parse import urlencode
from django.test import TestCase, Client
from rest_framework.test import APITestCase
from rest_framework.test import APIRequestFactory

from accounts import views
from accounts.models import User

from model_mommy import mommy

from rest_framework_jwt.settings import api_settings


class AccountAPITestCase(TestCase):
    
    def setUp(self):
        self.user = mommy.make(User, username='admin')
        self.user.set_password('admin')
        self.user.save()
        self.users = mommy.make(User, _quantity=10)
        jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
        jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER
        payload = jwt_payload_handler(self.user)
        self.token = jwt_encode_handler(payload)
        self.auth = 'JWT {0}'.format(self.token)
        self.client = Client()

    def test_users_create(self):
        data = {
            'username': 'test', 
            'email': 'test@test.com', 
            'password': 'test'
        }
        response = self.client.post('/users/create/', data=data, HTTP_AUTHORIZATION=self.auth)
        self.assertEquals(response.status_code, 201)


    def test_users(self):
        response = self.client.get('/users/', HTTP_AUTHORIZATION=self.auth)
        self.assertEquals(response.status_code, 200)
        self.assertEquals(len(response.json()), 11)
        self.assertEquals(response.json()[0]['username'], self.user.username)

    def test_users_detail(self):
        response = self.client.get('/users/{}/'.format(self.user.id), HTTP_AUTHORIZATION=self.auth)
        self.assertEquals(response.status_code, 200)
        self.assertEquals(response.json()['username'], self.user.username)

    def test_users_update(self):
        data = {'username': 'test', 'email': 'test@test.com'}
        content_type = 'application/json'
        response = self.client.patch('/users/update/{}/'.format(self.user.id), 
                                   json.dumps(data), content_type=content_type, 
                                   HTTP_AUTHORIZATION=self.auth)
        self.assertEquals(response.status_code, 200)
        self.assertEquals(response.json(), data)       
    
    def test_users_delete(self):
        response = self.client.delete('/users/delete/{}/'.format(self.user.id), HTTP_AUTHORIZATION=self.auth)
        self.assertEquals(response.status_code, 204)