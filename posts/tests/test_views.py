import json
from urllib.parse import urlencode
from django.test import TestCase, Client
from rest_framework.test import APITestCase
from rest_framework.test import APIRequestFactory

from accounts import views
from accounts.models import User
from posts.models import Post

from model_mommy import mommy

from rest_framework_jwt.settings import api_settings


class AccountAPITestCase(TestCase):
    
    def setUp(self):
        self.user = mommy.make(User, username='admin')
        self.user.set_password('admin')
        self.user.save()
        self.post = mommy.make(Post, title='Title test')
        jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
        jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER
        payload = jwt_payload_handler(self.user)
        self.token = jwt_encode_handler(payload)
        self.auth = 'JWT {0}'.format(self.token)
        self.client = Client()

    def test_posts_create(self):
        data = {
            'title': 'Title test', 
            'content': 'test content', 
        }
        response = self.client.post('/posts/create/', data=data, HTTP_AUTHORIZATION=self.auth)
        self.assertEquals(response.status_code, 201)

    def test_posts(self):
        response = self.client.get('/posts/', HTTP_AUTHORIZATION=self.auth)
        self.assertEquals(response.status_code, 200)
        self.assertEquals(len(response.json()), 1)
        self.assertEquals(response.json()[0]['title'], 'Title test')

    def test_posts_detail(self):
        response = self.client.get('/posts/{}/'.format(self.post.id), HTTP_AUTHORIZATION=self.auth)
        self.assertEquals(response.status_code, 200)
        self.assertEquals(response.json()['title'], self.post.title)

    def test_posts_update(self):
        data = {'title': 'test', 'content': 'content', 'id': 1}
        content_type = 'application/json'
        response = self.client.patch('/posts/update/{}/'.format(self.post.id), 
                                   json.dumps(data), content_type=content_type, 
                                   HTTP_AUTHORIZATION=self.auth)
        self.assertEquals(response.status_code, 200)
        self.assertEquals(response.json(), data)       
    
    def test_posts_delete(self):
        response = self.client.delete('/posts/delete/{}/'.format(self.post.id), HTTP_AUTHORIZATION=self.auth)
        self.assertEquals(response.status_code, 204)