from django.test import TestCase

from posts.models import Post

from model_mommy import mommy


class PostTestCase(TestCase):

    def setUp(self):
        self.post = mommy.make(Post)

    def test_post_creation(self):
        self.assertTrue(isinstance(self.post, Post))
        self.assertEquals(self.post.__str__(), self.post.title)