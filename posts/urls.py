from django.urls import path

from .views import PostCreateAPIView, PostListAPIView, PostRetrieveAPIView, PostUpdateAPIView, PostDestroyAPIView


urlpatterns = [
    path('', PostListAPIView.as_view(), name='posts'),
    path('<int:pk>/', PostRetrieveAPIView.as_view(), name='posts_detail'),
    path('create/', PostCreateAPIView.as_view(), name='posts_create'),
    path('update/<int:pk>/', PostUpdateAPIView.as_view(), name='posts_update'),
    path('delete/<int:pk>/', PostDestroyAPIView.as_view(), name='posts_delete'),
]
