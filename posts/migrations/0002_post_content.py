# Generated by Django 2.0.3 on 2018-03-29 21:08

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('posts', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='post',
            name='content',
            field=models.TextField(default=None, verbose_name='Conteúdo'),
            preserve_default=False,
        ),
    ]
