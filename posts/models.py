from django.db import models


class Post(models.Model):

    title = models.CharField('Títutlo', max_length=100)
    content = models.TextField('Conteúdo')

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Artigo'
        verbose_name_plural = 'Artigos'
        ordering = ['-id']